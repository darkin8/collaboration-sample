package org.example;

public class Main {
    public static void main(String[] args) {
        final String text = "Hello world!";
        final Integer num = 123;

        System.out.println(text + " " + num);
    }
}